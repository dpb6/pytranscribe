import os
from os.path import isfile, join
import speech_recognition as sr

# transcribe audio file
dir = os.path.dirname(os.path.abspath(__file__))
onlyfiles = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f)) and f.endswith('.wav')]

# use the audio file as the audio source
r = sr.Recognizer()
for audio_file in onlyfiles:
    print(audio_file)
    with sr.AudioFile(os.path.join(dir, audio_file)) as source:
        for i in range(7):
            audio = r.record(source, duration=60)
            print(r.recognize_google(audio), end=" ")
        print('')
