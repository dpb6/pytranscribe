# Transcription via Python and Google

## Background

http://www.ffmpeg.org/general.html#toc-Fraunhofer-AAC-library
Go to http://sourceforge.net/projects/opencore-amr/ and follow the instructions for installing the library. Then pass --enable-libfdk-aac to configure to enable it.

https://trac.ffmpeg.org/wiki/Encode/AAC
​Advanced Audio Coding (AAC) is the successor format to MP3, and is defined in MPEG-4 part 3 (ISO/IEC 14496-3). It is often used within an MP4 container format; for music the .m4a extension is customarily used. The second-most common use is within MKV (Matroska) files because it has better support for embedded text-based soft subtitles than MP4. The examples in this guide will use the extensions MP4 and M4A.

https://www.izotope.com/en/learn/whats-the-difference-between-file-formats.html
M4A files are encoded with the lossy Advanced Audio Coding (AAC) codec, which is able to provide the same bitrates as MP3s, yet achieve tighter compression.

https://pythonbasics.org/transcribe-audio/

https://realpython.com/python-speech-recognition/

## Docker image and commands to convert and transcribe

See https://github.com/sampeka/ffmpeg-python-docker

```
docker build -t ffmpeg-python-docker .

#docker run --rm -ti -v ${PWD}:/usr/src/app -w /usr/src/app ffmpeg-python-docker sh

#convert from m4a to wav
docker run --rm -ti -v ${PWD}:/usr/src/app -w /usr/src/app ffmpeg-python-docker ffmpeg -i GMT20201021-223655_Patricia-M.m4a GMT20201021-223655_Patricia-M.wav

docker run --rm -ti -v ${PWD}:/usr/src/app -w /usr/src/app ffmpeg-python-docker python transcribe.py

GMT20201021-223655_Patricia-M.wav
okay if I was imitating the solid structure as the baby's head I would first optimize my game and my focal phone if I felt like that still wasn't enough to optimized changing my death does not going to make it fill up enough of the screen so I would therefore use my zoom function and magnify that image I would undo further optimizations with gain or my focal Zone within that box image if I felt like that was Etta level word there was a good measurement I would freeze the image and then bring up annotation and annotate first there by invitation to as if it was the urinary bladder and because it was a little more superficial I may be able to bring up my focal Zone and adjust my death without having to zoom and loses much overall reference be able to optimize what I need to angle a little bit to increase my frame rate or my hurts and to crisp up those Porters and right now I'm looking at that being the bladder official structure I'm on harmonic high already I have good clear borders just to get a little more texture and surrounding structures increase my acoustic output still maintaining proper TI Mi and I would freeze my image is bladder and taking image if I was looking at more one of these structures as mimicking the bladder then again I would be changing my focal Zone we've got a lot of that was really the image up here that we don't need and so I would likely zoom on that part of the image optimized using my zone I'm still a good image quality and then annotate my image

docker system prune -a

rm GMT20201021-223655_Patricia-M.wav
```
