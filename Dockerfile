FROM sampeka/ffmpeg-python:3.6.5

RUN pip install --upgrade pip --trusted-host pypi.org --trusted-host files.pythonhosted.org

RUN apk add --no-cache --update flac

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/requirements.txt 
RUN pip install -r requirements.txt --trusted-host pypi.org --trusted-host files.pythonhosted.org
